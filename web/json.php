<?php

$filename = $_GET['file'] ?? null;

if ($filename) {
    header('Content-Type: application/json');
    echo file_get_contents("./json/$filename.json");
}