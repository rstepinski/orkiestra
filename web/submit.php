<?php
include('Pushbullet.php');

function error($e) {
    http_response_code(500);
    die;
}

if (isset($_POST['data'])) {
    $data = json_decode($_POST['data']);
}
else error('no data');

$host = getenv('DB_HOST');
$db = getenv('DB_DB');
$user = getenv('DB_USER');
$pass = getenv('DB_PASS');

$dsn = "mysql:host=$host;dbname=$db;charset=utf8";

try {
    $pdo = new PDO($dsn, $user, $pass);

    $sql = "INSERT INTO recordings (name, instrument, link, bar_from, bar_to) VALUES (?, ?, ?, ?, ?)";
    $statement = $pdo->prepare($sql);
    $statement->execute([
        $data->name,
        $data->instrument,
        $data->link,
        $data->from,
        $data->to
    ]);

    Pushbullet::sendLink('New recording', "Part: $data->instrument", $data->link);

    if ($statement->errorCode() !== '00000') error($statement->errorInfo());
}
catch (Exception $e) {
    error($e->getMessage());
}

echo 'ok';