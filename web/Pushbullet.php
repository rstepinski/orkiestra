<?php


class Pushbullet
{
    private static function api_key() {
        return getenv('PUSHBULLET_KEY');
    }

    public static function send($title, $body) {
        $data = ['title' => $title, 'body' => $body, 'type' => 'note'];
        self::_curlpush($data);
    }

    public static function sendLink($title, $body, $url) {
        $data = ['title' => $title, 'body' => $body, 'url' => $url, 'type' => 'link'];
        self::_curlpush($data);
    }

    private static function _curlpush($data) {
        if (!getenv('PUSHBULLET_EN', FALSE)) return;

        $json = json_encode($data);

        $headers = [
            'Content-Type: application/json',
            'Access-Token: '.self::api_key(),
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.pushbullet.com/v2/pushes');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close ($ch);
    }
}