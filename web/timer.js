this.events = null;

this.stopped = true;
this.pointer = 0;
this.leftover = 0;

this.started = 0;
this.current = 0;
this.expectedDuration = 0;

this.init = (events) => this.events = events;

this.clearAdjustmentTimers = () => {
  this.started = 0;
  this.current = 0;
  this.expectedDuration = 0;
};

this.start = () => {
  this.stopped = false;
  this.started = performance.now();
  this.expectedDuration = 0;
  this.tick();
};

this.stop = () => {
  this.stopped = true;
  this.clearAdjustmentTimers();
};

this.goto = (event) => {
  this.stop();
  this.pointer = event;
};

this.tick = () => {
  if (this.stopped) {
    this.clearAdjustmentTimers();
    return;
  }

  this.current = performance.now();
  let drift = this.current - this.started - this.expectedDuration;

  let delay = this.events[this.pointer++].duration;
  this.expectedDuration += delay;

  let intDelay = Math.floor(delay);
  this.leftover += delay - intDelay;

  this.postMessage('tick');
  if (this.leftover > 1) {
    let add = this.leftover - Math.floor(this.leftover);
    intDelay += add;
    this.leftover -= Math.floor(this.leftover);
  }
  if (this.leftover < 1) {
    let add = this.leftover - Math.ceil(this.leftover);
    intDelay += add;
    this.leftover -= Math.ceil(this.leftover);
  }

  intDelay = Math.round(intDelay - drift);
  setTimeout(() => {
    this.tick();
  }, intDelay);
};

this.addEventListener('message', e => {
  switch(e.data.type) {
    case 'init':
      this.init(e.data.events);
      break;

    case 'stop':
      this.stop();
      break;

    case 'start':
      this.start();
      break;

    case 'goto':
      this.goto(e.data.to);
      break;

    default:
  }
}, false);