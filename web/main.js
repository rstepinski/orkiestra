const urlParams = new URLSearchParams(window.location.search);
const name = urlParams.get('name') || 'kabalevsky';

function generateEventChain(data) {
  let bar_track = generateBarTrack(data);
  let events = [];
  let bar_click_map = [];

  let tempo = 120;
  let meter_n = 3;
  let meter_d = 4;
  let gradual_tempo_changes = [];
  for (let bar of bar_track) {
    if (data.meter_changes[bar]) {
      [meter_n, meter_d] = data.meter_changes[bar].split('/');
    }
    if (data.tempo_changes[bar]) {
      let tempo_change = data.tempo_changes[bar];
      if (typeof tempo_change == "number") tempo = tempo_change;
      else {
        for (let i = bar + 1; i <= data.bars; i++) {
          if (data.tempo_changes[i]) {
            gradual_tempo_changes.push({"from": bar, "to": i, "from_value": tempo, "to_value": data.tempo_changes[i]});
            break;
          }
        }
      }
    }

    bar_click_map[bar] = bar_click_map[bar] || [];
    bar_click_map[bar].push(events.length);
    let duration = calculateDuration(tempo, meter_d);
    for (let i = 1; i <= meter_n; i++) {
      events.push({
        "bar": bar,
        "measure": i,
        "duration": duration,
        "tempo": tempo,
        "numerator": meter_n,
        "denominator": meter_d,
        "strength": i == 1 ? "strong" : "weak",
      });
    }
  }

  for (let tempo_change of gradual_tempo_changes) {
    let event_from = bar_click_map[tempo_change.from][0];
    let event_to = bar_click_map[tempo_change.to][0] - 1;
    let event_d = event_to - event_from + 1;
    let tempo_d = (tempo_change.to_value - tempo_change.from_value) / event_d;

    for (let i = event_from; i <= event_to; i++) {
      let tempo = tempo_change.from_value + (i - event_from + 1) * tempo_d;
      events[i].duration = calculateDuration(tempo);
      events[i].tempo = Math.floor(tempo);
    }
  }

  let to_remove = [];
  for (let fermata of data.fermatas) {
    let event_from = bar_click_map[fermata.bar][0];

    if (fermata.measure_duration) {
      for (let i = 0; i < fermata.measure_duration - fermata.measure; i++) {
        to_remove.push(event_from + fermata.measure + i);
      }
    }

    let index = event_from + fermata.measure - 1;
    events[index].duration = fermata.duration;
    events[index].fermata = true;
  }

  let rehearsal_mark_events = [];
  const mark_type = data.rehearsal_mark_type;
  for (let i = 0; i < data.rehearsal_marks.length; i++) {
    const rehearsal_mark_bar = data.rehearsal_marks[i];
    for (let event_index of bar_click_map[rehearsal_mark_bar]) {
      rehearsal_mark_events.push({
        'event': event_index,
        'mark': mark_type === 'number' ? i + 1 : (mark_type === 'custom' ? data.custom_marks[i] : 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.substr(i, 1))
      });
    }
  }

  let rehearsal_mark = '';
  let rehearsal_mark_events_map = rehearsal_mark_events.map(e => e.event);
  for (let i = 0; i < events.length; i++) {
    if (rehearsal_mark_events_map.includes(i)) {
      events[i].rehearsal_mark_change = true;
      rehearsal_mark = rehearsal_mark_events.shift().mark;
    }
    events[i].rehearsal_mark = rehearsal_mark;
  }

  for (let index of to_remove) events[index] = null;

  events = events.filter(e => e !== null);

  let pickup_beats = [];
  let [pickup_n, pickup_d] = data.meter_changes[1].split('/');
  let pickup_tempo = data.tempo_changes[1];
  for (let i = 0; i < 2; i++) {
    for (let c = 1; c <= pickup_n; c++) {
      pickup_beats.push({
        "bar": -2 + i,
        "measure": c,
        "duration": calculateDuration(pickup_tempo, pickup_d),
        "tempo": pickup_tempo,
        "numerator": pickup_n,
        "denominator": pickup_d,
        "strength": c == 1 ? "strong" : "weak",
      });
    }
  }

  let last_bar = -2;
  let bar_click_map_no_repeats = [];
  for (let i = 0; i < events.length; i++) {
    if (last_bar != events[i].bar) {
      bar_click_map_no_repeats[events[i].bar] = i + pickup_beats.length;
      last_bar = events[i].bar;
    }
  }

  last_bar = -2;
  let bar_click_map_repeats = [];
  for (let i = 0; i < events.length; i++) {
    if (last_bar != events[i].bar) {
      if (!bar_click_map_repeats[events[i].bar]) bar_click_map_repeats[events[i].bar] = [];
      bar_click_map_repeats[events[i].bar].push(i + pickup_beats.length);
      last_bar = events[i].bar;
    }
  }

  return [[...pickup_beats, ...events], bar_click_map_repeats, bar_click_map_no_repeats];
}

function calculateDuration(bpm) {
  return (60 * 1000 / bpm);
}

function generateBarTrack(data) {
  let bar_track = [];
  let repeat_map = [];

  for (let repeat of data.repeats) {
    repeat_map[repeat.from] = repeat;
  }

  for (let i = 1; i <= data.bars; i++) {
    if (repeat_map[i]) {
      let repeat = repeat_map[i];
      if (repeat.verses) {
        let base_repeat_to = repeat.verses[0].from - 1;
        for (let verse of repeat.verses) {
          for (j = repeat.from; j <= base_repeat_to; j++) bar_track.push(j);
          for (j = verse.from; j <= verse.to; j++) bar_track.push(j);
        }
        for (j = repeat.from; j <= base_repeat_to; j++) bar_track.push(j);
      }
      else {
        let count = repeat.count || 2;

        for (c = 0; c < count; c++) {
          for (j = repeat.from; j <= repeat.to; j++) bar_track.push(j);
        }
      }

      i = repeat.to;
    }
    else bar_track.push(i);
  }

  return bar_track;
}

function generateRollBackground() {
  let pps = 100;

  let barline_events = [];
  let barlines = {
    strong: '<div class="barline strong"></div>',
    weak: '<div class="barline weak"></div>'
  };
  let total_displacement = 0;
  for (let event of event_chain) {
    const [strength, position] = [event.strength, event.duration * pps / 1000 - 2];
    total_displacement += position + 2;

    barline_events.push({
      displacement: total_displacement,
      duration: event.duration
    });

    $('#roll #tape').append(barlines[strength]);

    const last = $('#roll #tape div.barline:last-of-type');
    last.css('margin-right', position + 'px');

    if (strength == "strong") {
      if (event.rehearsal_mark_change) {
        last.append(`<div class="bar_number rehearsal">${event.rehearsal_mark}</div>`);
      }
      else if (event.bar % 5 == 1) {
        last.append(`<div class="bar_number">${event.bar}</div>`);
      }
    }
  }

  return barline_events;
}

function generateEntryRoll(entry_map) {
  for (const entry of entry_map) {
    let entry_bar = Array.isArray(entry[0]) ? entry[0][0] : entry[0];
    let entry_beat = Array.isArray(entry[0]) ? entry[0][1] : 1;
    let repeats = [1];
    let exit_bar = entry[1] + 1;

    if (typeof entry_bar == 'string') {
      repeats = entry_bar.split(':')[1].split(',');
      entry_bar = entry_bar.split(':')[0];
    }

    let entry_event = null;
    let exit_event = null;
    for (let r = 0; r < bar_click_map_repeats[entry_bar].length; r++) {
      let event = bar_click_map_repeats[entry_bar][r];
      if (repeats.includes(r + 1)) {
        entry_event = event + entry_beat - 2;
        let exit_events = bar_click_map_repeats[exit_bar];
        exit_event = exit_events[r] - 1;
      }
    }

    $('#tape').append('<div class="entry"></div>');
    let position = entry_event >= 0 ? barline_events[entry_event].displacement : 0;
    let width = barline_events[exit_event].displacement - position;
    $('#tape .entry:last-of-type').css('left', `${position}px`).css('width', width + 'px');
  }
}

let event_chain = null;
let bar_click_map = null;
let bar_click_map_repeats = null;
let barline_events = null;

let pointer = 0;
const worker = new Worker('timer.js');

const contextClass = (window.AudioContext || window.webkitAudioContext || window.mozAudioContext || window.oAudioContext || window.msAudioContext);
const context = new contextClass();
let buffers = {};

let loading = [];
function loadAudio(name) {
  loading.push(name);
  const request = new XMLHttpRequest();
  request.open('GET', `audio/${name}.mp3`, true);
  request.responseType = 'arraybuffer';
  request.onload = () => {
    context.decodeAudioData(request.response, buffer => {
      buffers[name] = buffer;
      loading = loading.filter(item => item != name);
      if (!loading.length) {
        $('.button#start').removeClass('loading');
      }
    });
  };
  request.send();
}
$('.button#start').addClass('loading');
loadAudio('strong');
loadAudio('weak');
loadAudio(name);

let baseOffset = 0;
let pianoSource = null;
let pianoEnabled = true;
_start = () => {
  if (loading.length) return;

  let startOffset = baseOffset;
  for (let i = 0; i < pointer; i++) {
    startOffset += event_chain[i].duration / 1000;
  }

  worker.postMessage({ type: 'start' });

  if (pianoEnabled) {
    pianoSource = context.createBufferSource();
    pianoSource.buffer = buffers[name];
    pianoSource.connect(context.destination);
    pianoSource.start(0, startOffset);
  }
};
_stop = () => {
  worker.postMessage({ type: 'stop' });
  if (pianoSource) pianoSource.stop();
};
_goto = (to) => worker.postMessage({ type: 'goto', to });

$('#music').click(() => {
  pianoEnabled = !pianoEnabled;
  $('#music').toggleClass('toggled');
});
$('#start').click(_start);
$('#stop').click(_stop);
$('#rewind').click(() => {
  _stop();
  pointer = 0;
  _goto(0);
  render();
  revertRoll();
});
$('#goto').click(() => {
  _stop();
  let event = bar_click_map[$('#bar_input').val()];
  pointer = event;
  _goto(event);
  render();
  revertRoll();
});

function tick() {
  sound();
  render();
  animateRoll();
  pointer++;
}

worker.addEventListener('message', e => {
  if (e.data == 'tick') tick();
});

function sound() {
  const source = context.createBufferSource();
  source.buffer = buffers[event_chain[pointer].strength];
  source.connect(context.destination);
  source.start(0);
}

function render() {
  let event = event_chain[pointer];
  $('#bar').text(event.bar);
  $('#beat').text(event.measure);
  $('#meter').text(event.numerator);
  $('#rehearsal_mark').text(event.rehearsal_mark);
  $('#tempo').text(event.tempo);

  if (event.fermata) {
    fermata(event.duration);
  }

  let note_map = {
    "1": "w",
    "2": "h",
    "4": "q",
    "8": "e",
    "16": "x"
  };
  $('#tempo_note').text(note_map[event.denominator]);
}

function fermata(duration) {
  $('#fermata_bar').removeClass('hidden');
  $('#fermata_bar').animate({ left: '100%' }, { duration, easing: 'linear' });

  setTimeout(() => {
    $('#fermata_bar').addClass('hidden');
    $('#fermata_bar').css('left', '0');
  }, duration + 100);
}

function revertRoll() {
  let d = (barline_events[pointer - 1] || {displacement: 0}).displacement;
  $('#tape').css('left', `-${d}px`);
}

function animateRoll() {
  let be = barline_events[pointer];
  let [d, duration] = [be.displacement, Math.floor(be.duration) - 15];
  $('#tape').animate({ left: `-${d}px` }, { duration, easing: 'linear'});
}

$.get(`/json.php?file=data_${name}`).then(response => {
  [event_chain, bar_click_map_repeats, bar_click_map] = generateEventChain(response);
  barline_events = generateRollBackground();

  worker.postMessage({ type: 'init', events: event_chain });
  /*$.get('/json.php?file=entry_map').then(response => {
    generateEntryRoll(response);
  })*/
});

$('.close').click(function() {
  $(this).parent().parent().addClass('hidden');
  localStorage.setItem('info-hide', 'true');
});
$('#btn-info').click(() => {
  $('#modal-info').removeClass('hidden');
  localStorage.removeItem('info-hide');
});
$('#btn-send').click(() => {
  $('#modal-send').removeClass('hidden');
  $('#form-success, #form-error').hide();
});

if (!localStorage.getItem('info-hide')) {
  $('#modal-info').removeClass('hidden');
}

$('#checkbox-all').click(() => $('#from-to').toggle());
$('button#submit').click(sendForm);

function sendForm() {
  const all = $('#checkbox-all').is(':checked');
  const data = {
    name: $('#input-name').val(),
    instrument: $('#input-instrument').val(),
    link: $('#input-link').val(),
    from: all ? 1 : $('#input-from').val(),
    to: all ? bar_click_map.length : $('#input-to').val()
  };

  const formError = () => { $('#form-error').show(); };
  const formSuccess = () => {
    $('#form-success').show();
    $('#modal-send input').val('');
  };

  $('#spinner').show();
  $.post('/submit.php', { data: JSON.stringify(data) }, response => {
      if (response === 'ok') formSuccess(); else formError();
      $('#spinner').hide();
  });
}

document.addEventListener('visibilitychange', () => {
  console.log(document.visibilityState);
  if (document.visibilityState == "hidden") _stop();
}, false);